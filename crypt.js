const bcrypt = require('bcrypt');

function hash(data) {
  console.log("Hashing data");

  // Encripta con 10 rondas de SALTeado
  return bcrypt.hashSync(data, 10);
}


function checkPassword(sentPassword, userHashedPassword) {
  console.log("Checking password");

  // Valida que el password enviado es el mismo que tenemos en la BBDD
  return bcrypt.compareSync(sentPassword, userHashedPassword);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
