const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');

chai.use(chaihttp);

var should = chai.should();
// Esto arranca y para la API para que se arranque automaticamente con esto de las pruebas unitarias
var server = require('../server');



// Agrupamos suits para agrupar los test
describe('First test',
	function() {
		// Es el codigo de la prueba propiamente dicho
		it('Test that Duck Duck works', function(done) {

			chai.request('http://www.duckduckgo.com')
			.get('/')
			.end(
				function(err, res) {
					console.log("Request has finished");
					res.should.have.status(200);
					// El done() especifica al test unitario cuando ha acabado la comprobacion para que lo sepa
					// Ponerlo siempre tras las comprobaciones
					done();
				}
			)
		})
	}
);

// Otro de integracion con nuestra API
describe('Second test',
	function() {
		it('Prueba que la API de usuario responde', function(done) {

			chai.request('http://localhost:3000')
			.get('/apitechu/v1/hello')
			.end(
				function(err, res) {
					console.log("Request has finished");
					res.should.have.status(200);
					done();
				}
			)
		},

		// El .be es una comprobacion de tipo
		// El .have es una comprobacion de esta o no esta
		it('Prueba que los usuarios estan definidos en el body y tienen la estructura deseada', function(done) {

			chai.request('http://localhost:3000')
			.get('/apitechu/v1/users')
			.end(
				function(err, res) {
					console.log("Request has finished");
					res.should.have.status(200);
					// Comprobacion de tipos: queremos comprobar que lo que esta devolviendo es un array
					res.body.users.should.be.a("array");

					console.log("BAQUI_1");

					for (user of res.body.users) {
						console.log("BAQUI_2");
						//user.should.have.property('email');
						//user.should.have.property('name');
						//user.should.have.property('third_name');
					}

					done();
				}
			)
		}
	}
)
