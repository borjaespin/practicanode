// Libreria con las funciones para escribir a fichero
const fs = require('fs');

// Sacamos a una funcion la escritura de los cambios a archivo
function writeUserDataToFile(data) {

  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
    if(err) {
      console.log(err);
    }
    else {
      console.log("Data written to disk");
    }
  });

}

// Esta linea quiere decir que este fichero de codigo pone disponible el writeUserDataToFile con el nombre writeUserDataToFile
module.exports.writeUserDataToFile = writeUserDataToFile;
