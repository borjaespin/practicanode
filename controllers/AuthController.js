// Metemos aqui la importacion del io y la quitamos del server.js porque alli ya no se utilizan
const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

// URL de base para componer las llamadas a la API REST
const baseMLabURL= "https://api.mlab.com/api/1/databases/apitechu/collections/";
const mLabAPIKey = "apiKey=deyXSJ7wCGU_Z631Fw4-w1zn5p82tkNu";


function loginV1(req, res) {
    console.log("POST /apitechu/v1/login");

    var validUsers = require('../validUsers.json');
    var output = "USUARIO NO ENCONTRADO";
    var result = {};

    for (const user of validUsers) {
      //console.log("Iterando en usuario " + user.email + " con password: " + user.password);

      // Comprobamos si el usuario existe
      if (user.email == req.body.email) {
           console.log("  Encontrado usuario valido: " + user.email);

           // Comprobamos si la contraseña coincide
           if (user.password == req.body.password) {
             console.log("  Contraseña coincide: " + user.password);
             // Añadimos la propiedad "logged" al usuario validado
             user.logged = true;
             //console.log(user);
             // Persistimos la informacion en fichero
             io.writeValidUserDataToFile(validUsers);

             result.id = user.id;
             output = "USUARIO LOGUEADO CORRECTAMENTE";

             break;
           }
           else {
             output = "CONTRASEÑA INCORRECTA";
           }
      }
    }

    result.message = output;
    res.send(result);
}


function logoutV1(req, res) {
    console.log("POST /apitechu/v1/logout");

    var validUsers = require('../validUsers.json');
    var output = "LOGOUT INCORRECTO, USUARIO NO ENCONTRADO";
    var result = {};

    for (const user of validUsers) {
      //console.log("Iterando en usuario " + user.email + " con password: " + user.password);

      // Comprobamos si el usuario existe
      if (user.id == req.body.id) {
           console.log("  Encontrado usuario por id: " + user.id);

           if (user.logged == true) {
             delete user.logged;
             // Persistimos la informacion en fichero
             io.writeValidUserDataToFile(validUsers);

             output = "LOGOUT CORRECTO";
           }
           else {
             output = "LOGOUT INCORRECTO, USUARIO NO LOGUEADO";
           }
      }
    }

    result.message = output;
    res.send(result);

}


function loginV2(req, res) {
  console.log("POST /apitechu/v2/login");

/*
  console.log("Primer nombre es: " + req.body.first_name);
  console.log("Apellido es: " + req.body.last_name);
  console.log("Email es: " + req.body.email);
  console.log("Password es: " + req.body.password);
*/

/* DESCOMENTAR y PICAR BIEN
  var httpClient = requestJson.createClient(baseMLabURL);
  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("query es " + query);

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {

          // No es necesario hashear el password que leemos desde el body de la peticion
          var isPasswordCorrect = crypt.checkPassword(req.body.password, body[0].password);
          console.log ("Password correcto");

          if (body.length == 0 || !isPasswordCorrect) {
            var response = {
              "mensaje" : "Login incorrecto, email y/o password no encontrados";
            }
          }

          res.send(response);

        } else {
          console.log ("Got a user with that email and password")
          query = 'q={"id" :' + body[0].id + '}';
          console.log("query es " + query);
          var putBody = '{"$set" : {"logged"}:true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT done");
              var response = {
              "msg" : "Usuario logado con exito",
              "idUsuario" : body[0].id
            }

            res.send(response);

        }
      )
    }
  }
  */
};



function logoutV2(req, res) {
  console.log("POST /apitechu/v2/logout/:id");


//TODO: COPIAR EL CODIGO DEL LOGIN Y ADECUARLO CON LOS CAMBIOS NECESARIOS PARA QUITAR LAS PROPIEDADES,
// SON LOS QUE VIENEN A CONTINUACION
/*
  var query = 'q={"id" : ' + req.params.id + '}';

  // Desetea la propiedad de logged en la coleccion
  var putBody = '{"$unset": {"logged":""}}'

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
  // More Stuff
  // Do shit
  httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),)

  */

}


module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
