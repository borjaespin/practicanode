// Metemos aqui la importacion del io y la quitamos del server.js porque alli ya no se utilizan
const io = require('../io');
const crypt = require('../crypt');

// URL de base para componer las llamadas a la API REST
const baseMLabURL= "https://api.mlab.com/api/1/databases/apitechu/collections/";
const mLabAPIKey = "apiKey=deyXSJ7wCGU_Z631Fw4-w1zn5p82tkNu";
// Metemos la libreria con sus funciones que vamos a usar
const requestJson = require('request-json');


function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  console.log("Query String");
  console.log(req.query.top);
  console.log(req.query.count);

  var result = {};

  // Cambiamos la ruta al json porque hemos movido este trozo de codigo a un js dentro de controllers
  var users = require('../usuarios.json');
  var topUsers = new Array();

  // Itero para coger los N primeros
  var i = 0;
  for (arrayId in users) {
       //console.log("posición del array es " + arrayId);
       if (i < req.query.$top && users[arrayId] != null) {
         //console.log("Usuario añadido: " + users[arrayId].first_name);
         topUsers.push(users[arrayId]);
       }
       ++i;

       if (i == req.query.$top) {
         break;
       }
  }
  result.users = topUsers;

  // Itero para contar los elementos de la lista devuelta
  req.query.$count == "true" ? result.count = topUsers.length : result.count = 'N/A';

  res.send(result);

/*
  for (i = 0; i < contUsers; i++) {
    // console.log("Iterando vez Nº " + i);
    console.log("Saco usuario: " + users[i]);
/*
    var newUser = {
      "id" :
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email
    };

    topUsers.push(newUser);
*/

/*
app.get("/apitechu/v1/users",
function(req, res) {
console.log("GET /apitechu/v1/users");
console.log(req.query);

var result = {};
var users = require('./usuarios.json');

if (req.query.$count == "true") {
console.log("Count needed");
result.count = users.length;
}

result.users = req.query.$top ?
users.slice(0, req.query.$top) : users;

res.send(result);
}
);

(Mini)PRÁCTICA

En GET USERS V1

Implementar los filtros $top y $count en el QUERY STRING de la petición

$top=5
-> Devuelve los 5 primeros usuarios

$count=true
-> Devuelve un campo "count" con el total de usuarios en la lista

! No el total de usuarios devueltos (pueden ser menos si se hace un top)

Se pueden combinar los dos

PISTA
Podemos devolver un object {} en lugar de un array []

Array.splice versus Array.slice

localhost:3000/apitechu/v1/users?$top=10&$count=true

*/



// Añadimos el objeto en el que vamos a dar la respuesta (res)
//res.send("RECIBIDO y CONTESTANDO, OVER");

// Ponemos una respuesta en formato json
//res.send('{"msg" : "Hola desde API de prueba TechU"}');
//Ahora lo enviamos como un json directamente en vez de como string
//res.send({"msg" : "Hola desde API de prueba TechU"});

/* Enviamos como respuesta un archivo entero, que hemos bajado de MOKAROO
 y metido dentro del directorio del proyecto; el root indica que coge desde
 el raiz del documento
*/
//res.sendFile('usuarios.json', {root: __dirname});

// Vamos a tratar el .json para mostrarlo de otra forma
//var users = require('./usuarios.json');
//res.send(users);
}

function createUserV1(req, res) {
  console.log("POST /apitechu/v1/users");
  // Vamos a coger el header para verlo en el log
  console.log(req.headers);

  // Buena practica: sacar un console.log de los parametros siempre
  // para verificar lo que estamos recibiendo (los hemos definido en el
  // headers del postman de la peticion POST /apitechu/v1/users)
  console.log("Primer nombre es: " + req.body.first_name);
  console.log("Apellido es: " + req.body.last_name);
  console.log("Email es: " + req.body.email);

  // Utilizamos un object para recoger los valores de la cabecera
  // y meterlos en el json de usuarios que tenemos
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  // Definimos el objeto de tipo array para simplemente hacerle
  // un push
  var users = require('../usuarios.json');
  users.push(newUser);

  // Usamos una variable de tuipo JSON para tragarnos el fichero de entrada
  // y tratarlo
  // Lo metemos en una funcion (writeUserDataToFile) puesto que lo vamos a
  // reutilizar
  /*
  var jsonUserData = JSON.stringify(users);
  fs.writeFile("./usuarios.json", jsonUserData, "utf8", function(err){
    if(err) {
      console.log(err);
    }
    else {
      res.send({"msg" : "Usuario añadido con exito"});
      console.log("Usuario añadido con exito");
    }
  });
  */
  io.writeUserDataToFile(users);

  //TRAMPA!!!!!
  // Lo enviamos como respuesta para ver que funciona, aunque no es
  // el objetivo de la funcion POST
  //res.send({"msg" : "Usuario añadido con exito"});
  //FIN_TRAMPA

}

function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:name");
  console.log("Se va a borrar el usuario: " + req.params.name);

  // Trampeamos el borrado: borramos el id recibido -1 en vez de recorrer
  // toda la lista y buscarlo
  var users = require ('../usuarios.json');
  // Funcion que devuelve un arrary sin los indices que le indicamos que quite
  // Con req.params recuperamos la entrada, con .id nos referimos al parametro
  // Borramos el -1 puesto que el array empieza en la pos 0
  //users.splice(req.params.id - 1, 1);
  // Y lo escribimos a disco
  //writeUserDataToFile(users);


  // PRACTICA: BORRAR UN ELEMENTO DE LA LISTA POR NOMBRE
  var cont = 0;
  for (const user of users) {
    //console.log("Iterando en usuario " + user.first_name + " con ID: " + user.id);

    // Buscamos el usuario con el nombre pasado por parametro
    if (user.first_name == req.params.name) {
      try {
        console.log("  Encontrado el usuario: " + user.first_name + " en el indice " + user.id);
        // Borramos el usuario por su indice y persistimos
        users.splice(cont, 1);
        io.writeUserDataToFile(users);
        console.log("Usuario borrado correctamente");
        break;
      }
      catch(err) {
        console.log("Error en el proceso de borrado " + err);
      }
    }

    // Actualizamos el contador con la posicion
    cont += 1;
  }

  // OTRAS OPCIONES PARA CODIFICAR ESTO
  /*
  for (user of users) {
       console.log("Length of array is " + users.length);
       if (user != null && user.id == req.params.id) {
         console.log("La id coincide");
         delete users[user.id - 1];
         // users.splice(user.id - 1, 1);
         break;
       }
     }

  for (arrayId in users) {
       console.log("posición del array es " + arrayId);
       if (users[arrayId].id == req.params.id) {
         console.log("La id coincide");
         users.splice(arrayId, 1);
         break;
       }
     }

  users.forEach(function (user, index) {
       if (user.id == req.params.id) {
         console.log("La id coincide");
         users.splice(index, 1);
       }
     });

  // Con lambdas
  var index = users.findIndex(x => x.id == req.params.id);
  users.splice(index,1);
    */



  res.send({"msg" : "Usuario borrado correctamente"});
}


function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  // Construimos la llamada GET a la API basandonos en lo que hemos creado con el postman
  // (crear primero la peticion en el postman)
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created succesfully");
  // Aqui es donde se especifica el verbo que vamos a utilizar para la peticion (GET en este caso)
  httpClient.get("user?" + mLabAPIKey,
      // OJO que la variable "res" la cambiamos de nombre para que no machaque el valor
      // de la "res" definida en el function dentro del que estamos
        function(err, resMLab, body) {
          var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
          }

          res.send(response);
        }
      )
}


function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  // Recogemos el parametros
  var id = req.params.id;
  // Importante: las comillas simples sirven para enviar cadenas literales (no es necesario escapar caracteres especiales)
  var query = 'q={"id":' + id + '}';

  // Construimos la llamada GET a la API basandonos en lo que hemos creado con el postman
  // (crear primero la peticion en el postman)
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created succesfully");
  // Aqui es donde se especifica el verbo que vamos a utilizar para la peticion (GET en este caso)
  httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            var response = {
              "msg" : "Error obteniendo usuario"
            }

            // Gestionamos a un grano mas fino los codigos de estado / error de la peticion
            // Por defecto devuelve el codigo 200 si no le indicamos nada,
            // si da error directamente, le indicamos que devuelva un 500
            res.status(500);

          } else {

          if (body.length > 0) {
            // Si la respuesta no es mala, siempre devuelve un array, por lo que asumimos esto
            var response = body[0];
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }

            res.status(404);
            }
          }

          res.send(response);
        }
      )
}

function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");
  console.log(req.headers);

  console.log("Primer nombre es: " + req.body.first_name);
  console.log("Apellido es: " + req.body.last_name);
  console.log("Email es: " + req.body.email);

  // Utilizamos un object para recoger los valores de la cabecera
  // y meterlos en el json de usuarios que tenemos
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    // ENCRIPTAMOS LA CONTRASEÑAS
    "password" : crypt.hash(req.body.password)
  };

  // Vemos la implementacion del API de MLab para estar seguro de como hacer la peticion post
  // vemos que en este caso el usuario hay que enviarlo en el body
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created succesfully");
  // Aqui es donde se especifica el verbo que vamos a utilizar para la peticion (GET en este caso)
  httpClient.post("user?" + mLabAPIKey, newUser,
      // OJO que la variable "res" la cambiamos de nombre para que no machaque el valor
      // de la "res" definida en el function dentro del que estamos
        function(err, resMLab, body) {
          console.log("Usuario creado con éxito");
          res.send({"msg" : "Usuario creado con exito"})
        })

      }


// Los exports van al final del todo
module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
