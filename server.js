// Definimos como constantes las variables globales
// Middleware para interpretar las URL's
const express = require('express');
// Libreria con las funciones para parsear el body
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;

// Añadimos todas las constantes que hemos sacado fuera del server.js tras refactorizar
// Definimos el io como el archivo io.js dentro del propio directorio
//const io = require('./io');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');


// Le indicamos en que formato viene el body para que sepa entenderlo
app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto ieshoras: " + port);

// Definimos una peticion GET para la ruta especificada
app.get('/apitechu/v1/hello',
        //asociandole una funcion handler, con estos parametros por convencion
        function(req, res) {
        // y especificamos su funcionamiento
          console.log("GET /apitechu/v1/hello");

        // Añadimos el objeto en el que vamos a dar la respuesta (res)
        //res.send("RECIBIDO y CONTESTANDO, OVER");

        // Ponemos una respuesta en formato json
        //res.send('{"msg" : "Hola desde API de prueba TechU"}');
        //Ahora lo enviamos como un json directamente en vez de como string
        res.send({"msg" : "Hola desde API de prueba TechU!!!"});
        }
      );

app.post("/apitechu/v1/monstruo/:p1/:p2",
        function(req, res) {

        console.log("POST /apitechu/v1/monstruo/:p1/:p2");

        console.log("Parametros");
        console.log(req.params);

        console.log("Query String");
        console.log(req.query);

        console.log("Headers");
        console.log(req.headers);

        console.log("Body");
        console.log(req.body);

        }
)


// A ESTO PARTE DE ABAJO SE LES LLAMA TABLAS DE ENRUTAMIENTO

// Definimos otra ruta para definir el recurso de usuarios
// Lo dejamos como una asociacion de la llamada con la funcion que la utiliza
app.get('/apitechu/v1/users', userController.getUsersV1);

// Creamos una ruta para dar de alta un usuario
app.post('/apitechu/v1/users', userController.createUserV1);

// Otra forma de enviar datos desde el cliente a el servidor
// app.delete('/apitechu/v1/users/:id',
// estamos pasando por parametro el id en la URL (:id)
app.delete('/apitechu/v1/users/:name', userController.deleteUserV1);


// PRACTICA PRACTITIONER LOGIN
app.post('/apitechu/v1/login', authController.loginV1);

// PRACTICA PRACTITIONER LOGOUT
app.post('/apitechu/v1/logout', authController.logoutV1);

// PRACTICA PRACTITIONER LOGIN CONTRA MLAB
app.post('/apitechu/v2/login', authController.loginV2);

// PRACTICA PRACTITIONER LOGOUT CONTRA MLAB
app.post('/apitechu/v2/logout/:id', authController.logoutV2);

// Refactorizamos el codigo
// Vamos a crear las llamadas a MLab desde aqui
app.get('/apitechu/v2/users', userController.getUsersV2);

// Otro ejemplo version 2 invocando a la API para obtener un usuario por ID
// Vamos a crear las llamadas a MLab desde aqui
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);

// Creamos una ruta para dar de alta un usuario
app.post('/apitechu/v2/users', userController.createUserV2);
